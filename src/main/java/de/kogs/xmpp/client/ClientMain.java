/**
 *
 */
package de.kogs.xmpp.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class ClientMain extends Application {
	
	public static void main(String[] args) {
		Application.launch(ClientMain.class, args);
	}
	
	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		primaryStage.setTitle("Login");
		
		Parent root = FXMLLoader.load(ClientMain.class.getResource("/loginForm.fxml"));

		Scene scene = new Scene(root);
		
		primaryStage.setScene(scene);
		primaryStage.show();

	}
	
}
