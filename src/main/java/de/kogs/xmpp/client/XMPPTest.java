package de.kogs.xmpp.client;


import rocks.xmpp.core.Jid;
import rocks.xmpp.core.roster.model.Contact;
import rocks.xmpp.core.session.ChatSession;
import rocks.xmpp.core.session.XmppSession;
import rocks.xmpp.core.stanza.model.client.Message;
import rocks.xmpp.core.stanza.model.client.Presence;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

/**
 *
 */

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class XMPPTest {
	
	public static void main(String[] args) {
		new XMPPTest();
	}
	

	public XMPPTest () {
		
//		XmppSessionConfiguration configuration = XmppSessionConfiguration.builder().debugger(new ConsoleDebugger()).build();
		
//		ConnectionConfiguration configuration = null;
//		try {
//			configuration = TcpConnectionConfiguration.builder().hostname("localhost").secure(true)
//					.sslContext(SSLContext.getDefault()).build();
//		} catch (NoSuchAlgorithmException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}

		final XmppSession xmppSession = new XmppSession("rmsv007");
		
//		xmppSession.addMessageListener(arg0 -> System.out.println(arg0.getMessage()));
		

//		xmppSession.addPresenceListener(e -> {
//
////			System.out.println(e.getPresence().getFrom());
//
//			if (e.isIncoming()) {
//					System.out.println(e.getPresence().getFrom() + " " + e.getPresence().isAvailable() + " "
//							+ e.getPresence().getStatus());
//			}
//		});


		// Listen for roster pushes
		xmppSession.getRosterManager().addRosterListener(e -> {
			
			List<Contact> allContacts = new ArrayList<Contact>();
			allContacts.addAll(e.getAddedContacts());
//			allContacts.addAll(e.getUpdatedContacts());
//			
//			allContacts.removeAll(e.getRemovedContacts());
			for (Contact con : allContacts) {
				System.out.println(con);

			}
			
		});

		try {
			xmppSession.connect();
		} catch (IOException e) {
			// e.g. UnknownHostException
			e.printStackTrace();
		}
		System.out.println("User:");
		Scanner in = new Scanner(System.in);
		String user = in.nextLine();
		System.out.println("Pw:");
		String pw = in.nextLine();

		try {
			xmppSession.login(user, pw, "jabber");
		} catch (FailedLoginException e) {
			// Login failed, due to wrong username/password
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		xmppSession.send(new Presence());
//		xmppSession.send(new Message(Jid.valueOf("mr1016@rmcan.com"), Message.Type.CHAT, "Sende mir mal ne nachricht"));
		int i = 0;
		while (xmppSession.isConnected()) {
			xmppSession.send(new Message(Jid.valueOf("ta3109@rmcan.com"), Message.Type.CHAT, "Hallo Tayfun"));

			i++;
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}

	}

	/**
	 * @param arg0
	 */
	private void sessionAdded(ChatSession chatSession) {
		

	}

}
