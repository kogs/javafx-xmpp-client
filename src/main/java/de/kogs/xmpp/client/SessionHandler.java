/**
 *
 */
package de.kogs.xmpp.client;

import de.kogs.xmpp.client.controller.ChatWindowController;
import de.kogs.xmpp.client.controller.ContactHolder;
import de.kogs.xmpp.client.controller.controls.InfoPopup;
import de.kogs.xmpp.client.controller.controls.InfoPopup.PopupType;
import javafx.application.Platform;
import rocks.xmpp.core.Jid;
import rocks.xmpp.core.XmppException;
import rocks.xmpp.core.roster.model.Contact;
import rocks.xmpp.core.session.TcpConnectionConfiguration;
import rocks.xmpp.core.session.XmppSession;
import rocks.xmpp.core.session.XmppSessionConfiguration;
import rocks.xmpp.core.stanza.MessageEvent;
import rocks.xmpp.core.stanza.MessageListener;
import rocks.xmpp.core.stanza.PresenceEvent;
import rocks.xmpp.core.stanza.PresenceListener;
import rocks.xmpp.core.stanza.model.client.Presence;
import rocks.xmpp.extensions.filetransfer.FileTransferManager;
import rocks.xmpp.extensions.vcard.temp.VCardManager;
import rocks.xmpp.extensions.vcard.temp.model.VCard;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import javax.security.auth.login.LoginException;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 */
public class SessionHandler {
	
	private static SessionHandler instance;
	
	public static SessionHandler getInstance() {
		return instance;
	}
	
	public static SessionHandler createInsanze(String user, String password, String server) throws LoginException,
			IOException {
		instance = new SessionHandler(user, password, server);
		return instance;
	}
	
	private XmppSession session;

	private HashMap<Jid, ContactHolder> contacts = new HashMap<Jid, ContactHolder>();

	private SessionHandler (String user, String password, String server) throws IOException, LoginException {
		
		XmppSessionConfiguration configuration = XmppSessionConfiguration.builder().defaultResponseTimeout(100).build();
		
		TcpConnectionConfiguration tcpConfiguration = TcpConnectionConfiguration.builder().secure(false).build();
		
		session = new XmppSession(server, configuration, tcpConfiguration);
		session.getDefaultTimeout();
		initSession();

		session.connect();
		System.out.println("connected");
		session.login(user, password, "test123");
		System.out.println("logged in");
		session.send(new Presence());
	}
	
	/**
	 * 
	 */
	private void initSession() {

		session.getRosterManager().addRosterListener(e -> {
			for (Contact con : session.getRosterManager().getContacts()) {

				ContactHolder holder = getContact(con.getJid());
				VCardManager vCardManager = session.getExtensionManager(VCardManager.class);

				try {
					VCard vCard = vCardManager.getVCard(holder.getJid());
					holder.setVCard(vCard);
				} catch (XmppException e1) {
					System.err.println("Error loading VCard for: " + holder.getJid() + " " + e1.getMessage());
				}

			}
		});
		session.addPresenceListener(new PresenceListener() {
			
			@Override
			public void handle(PresenceEvent p) {
				if (p.isIncoming()) {
					Presence per = p.getPresence();
					ContactHolder contact = getContact(per.getFrom());
					if (contact != null) {
						boolean hasPresence = contact.getPresence() != null;
						contact.setPresence(per);
						if (hasPresence) {
							Platform.runLater(() -> {
								InfoPopup infoPopup = new InfoPopup(contact, PopupType.STATUS);
								
							});
						}
						if (per.isAvailable()) {
							System.out.println(per.getFrom() + " is now Online");
						} else {
							System.out.println(per.getFrom() + " is now Offline");
						}

					}
				}
			}
		});
		
		session.addMessageListener(new MessageListener() {
			
			@Override
			public void handle(MessageEvent event) {
				if (event.isIncoming()) {
					if (event.getMessage().getFrom() != null) {
						Platform.runLater(() -> {
							ChatWindowController.showChat(event.getMessage().getFrom().asBareJid(), event.getMessage());
						});
					}
				}
			}
		});
		
		FileTransferManager fileTransferManager = session.getExtensionManager(FileTransferManager.class);
		fileTransferManager.addFileTransferOfferListener(e -> {
			ContactHolder sender = getContact(e.getInitiator());
			
			System.out.println("File " + e.getName() + " from " + sender.getContact().getName());
//			try {
//				OutputStream outputStream = new FileOutputStream("test.png");
//				
//				final FileTransfer fileTransfer = e.accept(outputStream);
//				fileTransfer.transfer();
//				
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}
			});

	}

	public XmppSession getSession() {
		return session;
	}
	
	public Collection<ContactHolder> getContactHolders() {
		return contacts.values();
	}
	
	public Collection<ContactHolder> getUnmodifableContactHolders() {
		return Collections.unmodifiableCollection(contacts.values());
	}

	public ContactHolder getContact(Jid jid) {
		Jid bareJid = jid.asBareJid();
		ContactHolder contactHolder = contacts.get(bareJid);
		if (contactHolder == null) {
			Contact contact = session.getRosterManager().getContact(jid);
			if (contact != null) {
				contactHolder = new ContactHolder(contact);
				contacts.put(bareJid, contactHolder);
			}
		}

		return contacts.get(bareJid);
	}

}
