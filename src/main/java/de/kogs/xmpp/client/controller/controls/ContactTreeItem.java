/**
 *
 */
package de.kogs.xmpp.client.controller.controls;

import de.kogs.xmpp.client.controller.ContactHolder;
import javafx.application.Platform;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 */
public class ContactTreeItem extends TreeItem<Object> {
	
	private ImageView onlineImage;
	private static String IMAGE_ONLINE_STYLE = "online";
	private static String IMAGE_OFFLINE_STYLE = "offline";

	public ContactTreeItem (ContactHolder holder) {
		super(holder);
		onlineImage = new ImageView();
		onlineImage.getStyleClass().add("connectionImage");
		if (holder.isOnline()) {
			onlineImage.getStyleClass().add(IMAGE_ONLINE_STYLE);
			onlineImage.getStyleClass().removeAll(IMAGE_OFFLINE_STYLE);
		} else {
			onlineImage.getStyleClass().add(IMAGE_OFFLINE_STYLE);
			onlineImage.getStyleClass().removeAll(IMAGE_ONLINE_STYLE);
		}

		setGraphic(onlineImage);
		holder.getIsOnlineProperty().addListener((obsv) -> {
			Platform.runLater(()->{
				if (holder.isOnline()) {
					onlineImage.getStyleClass().add(IMAGE_ONLINE_STYLE);
					onlineImage.getStyleClass().removeAll(IMAGE_OFFLINE_STYLE);
				} else {
					onlineImage.getStyleClass().add(IMAGE_OFFLINE_STYLE);
					onlineImage.getStyleClass().removeAll(IMAGE_ONLINE_STYLE);
				}
			});
		});
	}
}
