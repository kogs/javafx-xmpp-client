/**
 *
 */
package de.kogs.xmpp.client.controller;

import de.kogs.xmpp.client.SessionHandler;
import de.kogs.xmpp.client.controller.controls.ContactTreeItem;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 */
public class ContactsController implements Initializable {
	
	private static Stage contactsStage;
	
	public static void showContacts() {
		if (contactsStage == null) {
			contactsStage = new Stage();
			Parent root;
			try {
				FXMLLoader loader = new FXMLLoader(ContactsController.class.getResource("/contacts.fxml"));
				root = loader.load();
				ContactsController controller = loader.getController();
				contactsStage.focusedProperty().addListener((obs, oldV, newV) -> {
					if (newV) {
						controller.refresh();
					}
				});
				Scene scene = new Scene(root);
				scene.getStylesheets().add("clientStyle.css");
				contactsStage.setScene(scene);
			} catch (IOException e) {
				e.printStackTrace();
				contactsStage = null;
			}

		}
		contactsStage.show();
		contactsStage.toFront();
	}

	private SessionHandler instace;

	@FXML
	private Label userLabel;
	
	@FXML
	private ImageView userPic;
	
	@FXML
	private TextField filterTextField;
	
	@FXML
	private TreeView<Object> contactsView;
	TreeItem<Object> rootItem = new TreeItem<Object>("contacts");
	
	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		instace = SessionHandler.getInstance();

		rootItem.setExpanded(true);

		contactsView.setRoot(rootItem);
		
		contactsView.setOnMouseClicked((event) -> {
			ContactHolder selectedContact = null;
			if (!contactsView.getSelectionModel().isEmpty()) {
				Object selectedObject = contactsView.getSelectionModel().getSelectedItem().getValue();
				if (selectedObject instanceof ContactHolder) {
					selectedContact = (ContactHolder) selectedObject;
				}
			}
			if (selectedContact == null) {
				return;
			}

			if (event.getClickCount() == 2) {
				System.out.println("Open Chat Window for: " + selectedContact.getJid());
				ChatWindowController.showChat(selectedContact.getJid());
			}
			if (event.getButton().equals(MouseButton.SECONDARY)) {
				System.out.println("Info:");
				System.out.println(selectedContact.getJid());
				if (selectedContact.getPresence() != null) {
					System.out.println(selectedContact.getPresence().getShow());
					System.out.println(selectedContact.getPresence().getStatus());
				}
				
			}
		});

		userLabel.setText(instace.getSession().getConnectedResource().toString());
	}
	
	private synchronized void refreshContacts() {
		rootItem.getChildren().clear();

		String filterString = filterTextField.getText().toLowerCase();
		boolean filterActive = !filterString.trim().isEmpty();

		for (ContactHolder holder : instace.getUnmodifableContactHolders()) {
			ContactTreeItem item = new ContactTreeItem(holder);
			
			if (filterActive) {
				if (item.toString().toLowerCase().contains(filterString)) {
					rootItem.getChildren().add(item);
				}
			} else {
				rootItem.getChildren().add(item);
			}

		}
		rootItem.getChildren().sort(new ContactHolderComparator());
	}

	@FXML
	void filterContacts() {
		refreshContacts();
	}
	
	@FXML
	void refresh() {
		refreshContacts();
	}

}
