/**
 *
 */
package de.kogs.xmpp.client.controller.controls;



import de.kogs.xmpp.client.SessionHandler;
import de.kogs.xmpp.client.controller.ContactHolder;
import de.kogs.xmpp.client.controller.MessageParser;
import de.kogs.xmpp.client.util.JidUtil;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import rocks.xmpp.core.Jid;
import rocks.xmpp.core.stanza.MessageEvent;
import rocks.xmpp.core.stanza.MessageListener;
import rocks.xmpp.core.stanza.model.client.Message;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 */
public class ChatTab extends Tab implements Initializable {
	
	private SessionHandler instance;
	private ContactHolder contactHolder;

	@FXML
	private TextArea chatText;
	@FXML
	private TextFlow textFlow;
	@FXML
	private TextArea textSelection;
	@FXML
	private ScrollPane scrollPane;

	@FXML
	private Button smeilyButton;
	
	@FXML
	private Button sendButton;

	/**
	 * 
	 */
	public ChatTab (Jid jid) {
		instance = SessionHandler.getInstance();
		contactHolder = instance.getContact(jid);
		setText(contactHolder.getContact().getName());

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(ChatTab.class.getResource("/chatTab.fxml"));
		loader.setController(this);
		try {
			setContent(loader.load());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("chatTab loaded for: " + contactHolder.getJid());
		TextAreaButtonSkin skin = new TextAreaButtonSkin(chatText);
		chatText.setSkin(skin);
	
		MessageListener messageListener = new MessageListener() {
			
			@Override
			public void handle(MessageEvent event) {
				Message message = event.getMessage();
				getMessage(message);
			}
		};

		instance.getSession().addMessageListener(messageListener);
		setOnClosed((event) -> {
			instance.getSession().removeMessageListener(messageListener);
		});
		textSelection.setVisible(false);
		textSelection.setManaged(false);
		scrollPane.setOnMouseClicked((event) -> {
			textSelection.setText(getTextFromTextFlow());
			textSelection.setVisible(true);
			textSelection.setManaged(true);
		});

		textSelection.setOnMouseClicked((event) -> {
			copyTextSelection();
		});
		textSelection.focusedProperty().addListener((obs, oldV, newV) -> {
			if (!newV) {
				copyTextSelection();
			}
		});
		
		
		
	}

	private void copyTextSelection() {
		StringSelection selection = new StringSelection(textSelection.getSelectedText());
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(selection, selection);
		textSelection.setVisible(false);
		textSelection.setManaged(false);
	}

	private String getTextFromTextFlow() {
		String text = "";
		for (Node n : textFlow.getChildren()) {
			if (n instanceof Text) {
				text += ((Text) n).getText();
			} else if (n instanceof Hyperlink) {
				text += ((Hyperlink) n).getText();
			}
		}
		return text;
	}

	@FXML
	void keyPressed(KeyEvent event) {
		if (event.isShiftDown() && event.getCode().equals(KeyCode.ENTER)) {
			chatText.appendText("\n");
			event.consume();
		} else if (event.getCode().equals(KeyCode.ENTER)) {
			sendMessage();
			chatText.setText("");
			event.consume();
		}

	}
	
	@FXML
	void send() {
		
	}
	
	@FXML
	void smeily() {
		
	}

	public void getMessage(Message message) {
		if (JidUtil.isMessageFormOrToJid(message, contactHolder.getJid())) {
			
			Platform.runLater(() -> {
				if (message.getType() == Message.Type.CHAT) {
					String senderName;
					if (message.getFrom() == null) {
						senderName = "You";
					} else if (message.getFrom().asBareJid().equals(contactHolder.getJid().asBareJid())) {
						senderName = contactHolder.getContact().getName();
					} else {
						return;
					}
					
					Text sender = new Text("\n" + senderName + ": ");
					
					sender.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, 12));
					textFlow.getChildren().addAll(sender);
					textFlow.getChildren().addAll(MessageParser.parseMessage(message.getBody()));
					scrollPane.setVvalue(scrollPane.getVmax());
					textSelection.setText(getTextFromTextFlow());
				} else {

				}
			});
		}
	}
	
	private void sendMessage() {
		instance.getSession().send(new Message(contactHolder.getJid(), Message.Type.CHAT, chatText.getText()));
	}

}
