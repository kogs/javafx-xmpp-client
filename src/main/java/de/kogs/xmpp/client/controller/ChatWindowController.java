/**
 *
 */
package de.kogs.xmpp.client.controller;



import de.kogs.xmpp.client.controller.controls.ChatTab;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import rocks.xmpp.core.Jid;
import rocks.xmpp.core.stanza.model.client.Message;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class ChatWindowController implements Initializable {
	
	private static Stage chatWindow;
	private static ChatWindowController controller;
	
	public static void showChat(Jid jid) {
		showChat();
		controller.showChatForJid(jid);
	}
	
	/**
	 * @param asBareJid
	 * @param message
	 */
	public static void showChat(Jid jid, Message message) {
		showChat();
		controller.showChatForJid(jid, message);
		
	}

	public static void showChat() {
		if (chatWindow == null) {
			chatWindow = new Stage();
			chatWindow.initStyle(StageStyle.UNIFIED);
			Parent root;
			try {
				FXMLLoader loader = new FXMLLoader(ContactsController.class.getResource("/chatWindow.fxml"));
				root = loader.load();
				controller = loader.getController();
				

				Scene scene = new Scene(root);
				scene.setFill(Color.TRANSPARENT);
				scene.getStylesheets().add("clientStyle.css");
				chatWindow.setScene(scene);
			} catch (IOException e) {
				e.printStackTrace();
				chatWindow = null;
			}
			
		}
		chatWindow.show();
		chatWindow.toFront();
	}
	
	public static void hideChat() {
		chatWindow.hide();
	}

	@FXML
	private TabPane tabPane;

	private HashMap<Jid, ChatTab> tabs = new HashMap<Jid, ChatTab>();

	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		
	}
	
	
	private void showChatForJid(Jid jid){
		showChat(jid, null);
	}
	private void showChatForJid(Jid jid,Message message) {
		ChatTab chatTab = tabs.get(jid);
		if (chatTab == null) {
			chatTab = new ChatTab(jid);
			if (message != null) {
				chatTab.getMessage(message);
			}
			tabPane.getTabs().add(chatTab);
			tabs.put(jid, chatTab);
			chatTab.setOnClosed((event)->{
				tabs.remove(jid);
				if (tabs.size() == 0) {
					hideChat();
				}
			});
		}
		tabPane.getSelectionModel().select(chatTab);
		
	}


}
