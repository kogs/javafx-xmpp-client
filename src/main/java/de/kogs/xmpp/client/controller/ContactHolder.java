/**
 *
 */
package de.kogs.xmpp.client.controller;


import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import rocks.xmpp.core.Jid;
import rocks.xmpp.core.roster.model.Contact;
import rocks.xmpp.core.stanza.model.client.Presence;
import rocks.xmpp.extensions.vcard.temp.model.VCard;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class ContactHolder implements Comparable<ContactHolder> {
	
	private Contact contact;
	private Presence presence;
	
	private BooleanProperty isOnlineProperty = new SimpleBooleanProperty(false);
	private VCard vCard;


	public ContactHolder (Contact contact) {
		this.contact = contact;
	}

	public Jid getJid() {
		return contact.getJid();
	}
	

	public Contact getContact() {
		return contact;
	}
	
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contact == null) ? 0 : contact.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ContactHolder other = (ContactHolder) obj;
		if (contact == null) {
			if (other.contact != null) {
				return false;
			}
		} else if (!contact.equals(other.contact)) {
			return false;
		}
		return true;
	}

	public Boolean isOnline() {
		return isOnlineProperty.get();
	}

	public void setIsOnline(Boolean isOnline) {
		this.isOnlineProperty.set(isOnline);
	}

	public BooleanProperty getIsOnlineProperty() {
		return isOnlineProperty;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return contact.getName();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ContactHolder o) {
		if(isOnline() && !o.isOnline()){
			return 1;
		}else if(!isOnline() && o.isOnline()){
			return -1;
		}else{
			toString().compareTo(o.toString());
		}
		
		
		return 0;
	}

	public Presence getPresence() {
		return presence;
	}

	public void setPresence(Presence presence) {
		this.presence = presence;
		setIsOnline(presence.isAvailable());
	}
	
	public void setVCard(VCard vCard) {
		this.vCard = vCard;
	}
	
	public VCard getVCard() {
		return vCard;
	}

}
