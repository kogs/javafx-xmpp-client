/**
 *
 */
package de.kogs.xmpp.client.controller.controls;

import de.kogs.xmpp.client.controller.ContactHolder;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 */
public class InfoPopup extends Stage implements Initializable {
	private static int showAmountOfStatus = 0;
	
	private PopupType popupType;
	private ContactHolder holder;

	private Point2D pos;

	public enum PopupType {
		STATUS, INFO;
	}
	
	public InfoPopup (ContactHolder holder, PopupType type) {
		this.holder = holder;
		this.popupType = type;
		try {
			init();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public InfoPopup (ContactHolder holder, Point2D pos) {
		this.holder = holder;
		this.popupType = PopupType.INFO;
		this.pos = pos;
		try {
			init();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void init() throws IOException {
		initStyle(StageStyle.UTILITY);
		Parent root = null;
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setController(this);
		if (popupType == PopupType.STATUS) {
			setWidth(200);
			setHeight(100);
			fxmlLoader.setLocation(this.getClass().getResource("/statusPopup.fxml"));
			
		} else if (popupType == PopupType.INFO) {
			fxmlLoader.setLocation(this.getClass().getResource("/infoPopup.fxml"));
		} else {
			throw new IllegalArgumentException("Popup Type " + popupType + " is not assigned");
		}
		root = fxmlLoader.load();
		
		Scene scene = new Scene(root);
		setScene(scene);
		setAlwaysOnTop(true);
	}
	
	@FXML
	private Label userName;
	
	@FXML
	private Label status;

	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		if (popupType == PopupType.STATUS) {
			userName.setText(holder.getContact().getName());
			if (holder.getPresence().isAvailable()) {
				status.setText("Is Online now");
			} else {
				status.setText("Is Offline now");
			}
			
			Rectangle2D visiualBounds = Screen.getPrimary().getVisualBounds();
			Rectangle2D bounds = Screen.getPrimary().getBounds();
			double endY = visiualBounds.getHeight() - getHeight() - (showAmountOfStatus * getHeight());
			double startY = bounds.getHeight()+5;
			
			setY(startY);
			setX(visiualBounds.getWidth() - getWidth());

			DoubleProperty yProp = new SimpleDoubleProperty(getY());
			yProp.addListener((obs) -> setY(yProp.get()));
			
			Timeline timeline = new Timeline();
			timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1), new KeyValue(yProp, endY)));
			timeline.play();
			
			PauseTransition pauseTransition = new PauseTransition(Duration.seconds(5));
			pauseTransition.setOnFinished((action) -> {
				close();
				showAmountOfStatus--;
			});
			timeline.setOnFinished((ac) -> pauseTransition.play());
			showAmountOfStatus++;
		} else if (popupType == PopupType.INFO) {
			setX(pos.getX());
			setY(pos.getY());


		}
		show();
	}
	
}
