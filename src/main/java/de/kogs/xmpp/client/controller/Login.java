/**
 *
 */
package de.kogs.xmpp.client.controller;

import de.kogs.xmpp.client.SessionHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.security.auth.login.LoginException;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class Login implements Initializable {
	@FXML
	private PasswordField passwordTextField;
	
	@FXML
	private TextField userTextfield;
	
	@FXML
	private TextField serverTextField;
	

	
	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	@FXML
	void login() {
		String user = userTextfield.getText();
		String pass = passwordTextField.getText();
		String server = serverTextField.getText();
		
		if (!user.isEmpty() && !pass.isEmpty() && !server.isEmpty()) {
			try {
				SessionHandler.createInsanze(user, pass, server);
				ContactsController.showContacts();

			} catch (LoginException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			
		}

	}

}
