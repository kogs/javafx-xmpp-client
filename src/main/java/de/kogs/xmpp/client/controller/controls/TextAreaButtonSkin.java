/**
 *
 */
package de.kogs.xmpp.client.controller.controls;

import com.sun.javafx.scene.control.skin.TextAreaSkin;

import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Region;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class TextAreaButtonSkin extends TextAreaSkin {
	
	private ScrollPane scrollPane;
	private Region r;
	

	/**
	 * @param textArea
	 */
	public TextAreaButtonSkin (TextArea textArea) {
		super(textArea);
		for (Node n : getChildren()) {
			if (n instanceof ScrollPane) {
				scrollPane = (ScrollPane) n;
			} else if (n instanceof Region) {


			}
		}
		r = (Region) scrollPane.getContent();
		scrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
	}
	
	public ScrollPane getScrollPane() {
		return scrollPane;
	}

}
