/**
 *
 */
package de.kogs.xmpp.client.controller;

import java.util.Comparator;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class ContactHolderComparator implements Comparator<Object> {
	
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object o1, Object o2) {
		if (o1 instanceof ContactHolder && o2 instanceof ContactHolder) {
			((ContactHolder) o1).compareTo((ContactHolder) o2);
		}
		return 0;
	}
	
}
