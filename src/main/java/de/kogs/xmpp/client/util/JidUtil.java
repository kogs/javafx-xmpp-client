/**
 *
 */
package de.kogs.xmpp.client.util;

import rocks.xmpp.core.Jid;
import rocks.xmpp.core.stanza.model.client.Message;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class JidUtil {
	
	public static boolean isMessageFormOrToJid(Message message, Jid jid) {
		
		if (message.getFrom() != null && message.getFrom().asBareJid().equals(jid.asBareJid())) {
			return true;
		}
		
		if (message.getTo() != null && message.getTo().asBareJid().equals(jid.asBareJid())) {
			return true;
		}
		
		return false;
	}

}
